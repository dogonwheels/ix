ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',
  'game.geometry',
  'game.v2',
  'game.floor',
  'game.entities.player',
  'game.entities.logo',
  'game.levels.room',
  'game.levels.generator'
)
.defines(function () {
  /*Math.random.seed = 53949;
  Math.random = function() {
    var x = Math.sin(Math.random.seed++) * 10000;
    return x - Math.floor(x);
  };*/

  GameFloor = ig.Game.extend({
    instructText: new ig.Font('media/04b03.font.png'),

    init: function() {
      //Math.random.seed = ig.worldSeed;
      var levelLength = Math.floor((ig.level / 3) + 2);
      var generator = new CellGenerator(levelLength);

      //createLevel(this);
      this.entities = [];

      var mapWidthInRooms = generator.extents.x;
      var mapHeightInRooms = generator.extents.y;

      var roomDims = new V2(22, 17);

      var layers = {
        collisionData: new Array2d(new V2(44 * mapWidthInRooms, 34 * mapHeightInRooms), 0),
        floorData: new Array2d(new V2(22 * mapWidthInRooms, 17 * mapHeightInRooms), 0),
        wallData: new Array2d(new V2(22 * mapWidthInRooms, 17 * mapHeightInRooms), 0),
        ceilingData: new Array2d(new V2(22 * mapWidthInRooms, 17 * mapHeightInRooms), 0)
      };

      var rooms = new Array2d(generator.extents, null);
      var startingRoom;
      _(generator.cells).each(function (cell) {
        var position = new V2(cell.position.x, cell.position.y);
        if (!rooms.get(position)) {
          rooms.set(position, new Room(position, roomDims, cell.doors, cell.distance, layers));

          if (cell.distance === 0) {
            console.log("starting at", position);
            startingRoom = position;
          }
        }
      });

      this.collisionMap = new ig.CollisionMap(8, layers.collisionData.data);
      var renderCollisionMap = new ig.BackgroundMap(8, layers.collisionData.data, 'media/visible-collisions.png');
      var floorMap = new ig.BackgroundMap(16, layers.floorData.data, 'media/room-tiles.png');
      var wallMap = new ig.BackgroundMap(16, layers.wallData.data, 'media/room-tiles.png');
      var ceilingMap = new ig.BackgroundMap(16, layers.ceilingData.data, 'media/room-tiles.png');
      ceilingMap.foreground = true;

      this.backgroundMaps = [];
      this.backgroundMaps.push(floorMap);
      this.backgroundMaps.push(wallMap);
      this.backgroundMaps.push(ceilingMap);
//      this.backgroundMaps.push(renderCollisionMap);

      var tileSize = 16;
      var roomSize = new V2(22, 17);

      // Call post-init ready function on all entities
      for( var i = 0; i < this.entities.length; i++ ) {
        this.entities[i].ready();
      }

      this.floor = new Floor(rooms, startingRoom, roomSize);

      var cameraPos = startingRoom.mult(roomSize).add(new V2(1, 1)).scale(tileSize);
      this.screen.x = cameraPos.x;
      this.screen.y = cameraPos.y;

      this.floor.start();

      ig.input.bind(ig.KEY.LEFT_ARROW, 'left');
      ig.input.bind(ig.KEY.RIGHT_ARROW, 'right');
      ig.input.bind(ig.KEY.UP_ARROW, 'up');
      ig.input.bind(ig.KEY.DOWN_ARROW, 'down');

      ig.input.bind(ig.KEY.A, 'fireleft');
      ig.input.bind(ig.KEY.D, 'fireright');
      ig.input.bind(ig.KEY.W, 'fireup');
      ig.input.bind(ig.KEY.S, 'firedown');
    },

    died: function() {
      ig.lives -= 1;
      if (ig.lives < 0) {
        ig.lives = 0; // To prevent showing -1 momentarily
        ig.system.setGame(GameOverScreen);
      } else {
        this.floor.respawn();
      }
    },

    draw: function () {
      this.parent();
      this.instructText.draw('Mana: ' + ig.game.ammo, 20, 8, ig.Font.ALIGN.LEFT);
      this.instructText.draw('Lives: ' + ig.lives, 300, 8, ig.Font.ALIGN.RIGHT);
      this.instructText.draw('Gold: ' + ig.gold, 20, 18, ig.Font.ALIGN.LEFT);
      this.instructText.draw('Level: ' + ig.level, 300, 18, ig.Font.ALIGN.RIGHT);
    },

    update: function () {
  		// Chase the player to the next room
      var player = this.getEntitiesByType(EntityPlayer)[0];
      if (player && player.autopilot) {
        var speed = 4;
        var destination = new V2(
          player.destinationPos.x - ig.system.width / 2,
          player.destinationPos.y - ig.system.height / 2
        );

        var move = moveTowards(this.screen, destination);

        this.screen.x += move.x * speed;
        this.screen.y += move.y * speed;
      }

      // Follow player
//      this.screen.x = player.pos.x - ig.system.width / 2;
//      this.screen.y = player.pos.y - ig.system.height / 2;

      this.floor.update();

      this.parent();
    }
  });

    LogoScreen = ig.Game.extend({
      splashTime: new ig.Timer(2.4),
      clearColor: "#140D1B",
      init: function () {
        ig.game.spawnEntity(EntityLogo, 144, 100);
      },
      update: function () {
        if (this.splashTime.delta() > 0) {
          ig.system.setGame(GameStartScreen);
        } else {
          this.parent();
        }
      }
    });

    GameStartScreen = ig.Game.extend({
      font: new ig.Font('media/04b03.font.png'),
      splash: new ig.Image('media/splash.png'),
      clearColor: "#140D1B",
      init: function () {
        ig.input.bind(ig.KEY.SPACE, 'start');
      },
      update: function () {
        if (ig.input.pressed('start')) {
          ig.lives = 3;
          ig.level = 1;
          ig.gold = 0;
          ig.system.setGame(GameFloorStartScreen);
        }
        this.parent();
      },
      draw: function () {
        this.parent();
        this.splash.draw(98, 8);

        for (var i = 0; i < 6; i++) {
          var leftString = (i+1) + ". " + "---";
          var rightString = "Level: - Gold: -";

          var result = ig.leaderboard.results[i];
          if (result) {
            var when = new Date(result.when);
            leftString = (i+1) + ". " + when.getDate() + "/" + when.getMonth() + "/" + when.getFullYear() + " " + when.getHours() + ":" + when.getMinutes();
            rightString = "Level: " + result.level + " Gold: " + result.gold;
          }

          this.font.draw(leftString, 40, 100 + (15 * i), ig.Font.ALIGN.LEFT);
          this.font.draw(rightString, 280, 100 + (15 * i), ig.Font.ALIGN.RIGHT);
        }

        this.font.draw('Arrows to move. WASD to shoot.', 160, 205, ig.Font.ALIGN.CENTER);
        this.font.draw('Press spacebar to start', 160, 220, ig.Font.ALIGN.CENTER);
      }
    });

    GameFloorStartScreen = ig.Game.extend({
      font: new ig.Font('media/04b03.font.png'),
      clearColor: "#140D1B",
      init: function () {
        ig.input.bind(ig.KEY.SPACE, 'start');
      },
      update: function () {
        if (ig.input.pressed('start')) {
          ig.system.setGame(GameFloor);
        }
        this.parent();
      },
      draw: function () {
        this.parent();
        this.font.draw('LEVEL ' + ig.level, 160, 90, ig.Font.ALIGN.CENTER);
        this.font.draw('Press spacebar to continue', 160, 150, ig.Font.ALIGN.CENTER);
      }
    });

    GameCompleteScreen = ig.Game.extend({
      font: new ig.Font('media/04b03.font.png'),
      clearColor: "#140D1B",
      init: function () {
        ig.input.bind(ig.KEY.SPACE, 'continue');
      },
      update: function () {
        if (ig.input.pressed('continue')) {
          ig.system.setGame(GameStartScreen);
        }
        this.parent();
      },
      draw: function () {
        this.parent();
        this.font.draw('CONGRATULATIONS!', 160, 100, ig.Font.ALIGN.CENTER);
        this.font.draw('Press spacebar to continue', 160, 140, ig.Font.ALIGN.CENTER);
      }
    });


    GameOverScreen = ig.Game.extend({
      font: new ig.Font('media/04b03.font.png'),
      clearColor: "#140D1B",
      init: function () {
        ig.input.bind(ig.KEY.SPACE, 'continue');

        var results = ig.leaderboard.results;
        results.push({ when: (new Date()).getTime(), level: ig.level, gold: ig.gold });
        results = _(results).sortBy(function (r) { return -r.gold; }).slice(0, 6);

        ig.leaderboard.results = results;
        window.localStorage.leaderboard = JSON.stringify(ig.leaderboard);
      },
      update: function () {
        if (ig.input.pressed('continue')) {
          ig.system.setGame(GameStartScreen);
        }
        this.parent();
      },
      draw: function () {
        this.parent();
        this.font.draw('GAME OVER', 160, 100, ig.Font.ALIGN.CENTER);
        this.font.draw('Press spacebar to continue', 160, 140, ig.Font.ALIGN.CENTER);
      }
    });

    var leaderboardJson = window.localStorage.leaderboard;
    if (leaderboardJson) {
      ig.leaderboard = JSON.parse(leaderboardJson);
    } else {
      ig.leaderboard = { results: [] };
    }

    ig.worldSeed = 42;
    ig.lives = 3;
    ig.level = 1;
    ig.gold = 0;
    ig.completedFloor = function () {
      ig.level += 1;

      if (ig.level === 100) {
        ig.system.setGame(GameCompleteScreen);
      } else {
        ig.system.setGame(GameFloorStartScreen);
      }
    };

  // Start the Game with 60fps, a resolution of 320x240, scaled
  // up by a factor of 2
  ig.main( '#canvas', LogoScreen, 60, 320, 240, 2 );
//  ig.main( '#canvas', GameStartScreen, 60, 320, 240, 2 );
//  ig.main( '#canvas', GameFloor, 60, 320, 240, 2 );
//  ig.main( '#canvas', GameFloor, 60, 640, 480, 1 );
//  ig.main( '#canvas', GameFloor, 60, 1280, 1280, 0.5 );
});
