ig.module(
  'game.levels.room'
)
.requires(
  'game.constants',
  'game.array2d',
  'game.levels.wavegenerator',
  'game.entities.chalice',
  'game.entities.exit',
  'game.entities.spawn'
)
.defines(function() {
  Room = ig.Class.extend({
    locked: true,

    init: function (roomPosition, roomDims, doors, distance, layers) {
      this.roomPosition = roomPosition;
      this.roomDims = roomDims;
      this.layers = layers;

      this.doorCollisionAreas = [];

      var collisionData = new Array2d(roomDims.scale(2));
      var floorData = new Array2d(roomDims);
      var wallData = new Array2d(roomDims);
      var ceilingData = new Array2d(roomDims);
      this.exitIndicatorsData = new Array2d(roomDims);
      this.spawnAreas = [];

      var origin = new V2(0, 0);

      floorData.fill(origin, roomDims, tiles.floorBlue);
      ceilingData.stroke(origin, roomDims, tiles.ceiling);

      // Top wall
      collisionData.fill(origin, new V2(roomDims.x * 2, 5), tiles.collide.block);
      ceilingData.hline(new V2(1, 1), 20, tiles.tallWallNWUpper, tiles.tallWallNUpper, tiles.tallWallNEUpper);
      wallData.hline(new V2(1, 2), 20, tiles.tallWallNWLower, tiles.tallWallNLower, tiles.tallWallNELower);

      var decalPositions = _.range(2, 20);
      var bricks = _.range(0, 4);

      var top = _(decalPositions).shuffle().slice(0, bricks.random());
      var bottom = _(decalPositions).shuffle().slice(0, bricks.random());


      _(top).each(function (x) {
        ceilingData.set(new V2(x, 1), tiles.tallWallNUpperAlt);
      });
      _(bottom).each(function (x) {
        wallData.set(new V2(x, 2), tiles.tallWallNLowerAlt);
      });

      // Bottom wall
      collisionData.fill(new V2(0, (roomDims.y - 1) * 2), new V2(roomDims.x * 2, 2), tiles.collide.block);
      ceilingData.hline(new V2(1, (roomDims.y - 2)), 20, tiles.tallWallSW, tiles.tallWallS, tiles.tallWallSE);

      // Left wall
      collisionData.fill(origin, new V2(4, roomDims.y * 2), tiles.collide.block);
      ceilingData.vline(new V2(1, 3), roomDims.y - 5, tiles.tallWallW);

      // Right wall
      collisionData.fill(new V2((roomDims.x * 2) - 4, 0), new V2(4, roomDims.y * 2), tiles.collide.block);
      ceilingData.vline(new V2(roomDims.x - 2, 3), roomDims.y - 5, tiles.tallWallE);

      var hDoorStart = 9;
      var hDoorWidth = 4;
      var vDoorStart = 7.5;
      var vDoorHeight = 3;

      var collision = {
        top: { pos: new V2(hDoorStart * 2, 4), dims: new V2(hDoorWidth * 2, 1) },
        bottom: { pos: new V2(hDoorStart * 2, (roomDims.y - 1) * 2), dims: new V2(hDoorWidth * 2, 1) },
        left: { pos: new V2(3, vDoorStart * 2), dims: new V2(1, vDoorHeight * 2) },
        right: { pos: new V2((roomDims.x * 2) - 4, vDoorStart * 2), dims: new V2(1, vDoorHeight * 2) }
      };

      var spawnAreas = {
        top: { pos: new V2(hDoorStart, -1), dims: new V2(hDoorWidth, 2) },
        bottom: { pos: new V2(hDoorStart, roomDims.y - 1), dims: new V2(hDoorWidth, 2) },
        left: { pos: new V2(-1, vDoorStart), dims: new V2(2, vDoorHeight) },
        right: { pos: new V2(roomDims.x - 1, vDoorStart), dims: new V2(2, vDoorHeight) }
      };

      var weaponSpawnPositions = [
        new V2(3, 4),
        new V2(9, 4),
        new V2(13, 4),
        new V2(19, 4),
        new V2(3, 7),
        new V2(19, 7),
        new V2(3, 11),
        new V2(19, 11),
        new V2(3, 14),
        new V2(9, 14),
        new V2(13, 14),
        new V2(19, 14)
      ];
      this.weaponSpawnPositions = _(weaponSpawnPositions).shuffle().slice(0, 4);

      this.bumperAreas = {
        top: { pos: new V2(hDoorStart * 2, -3), dims: new V2(hDoorWidth * 2, 1) },
        bottom: { pos: new V2(hDoorStart * 2, (roomDims.y * 2) + 2), dims: new V2(hDoorWidth * 2, 1) },
        left: { pos: new V2(-3, vDoorStart * 2), dims: new V2(1, vDoorHeight * 2) },
        right: { pos: new V2((roomDims.x * 2) + 2, vDoorStart * 2), dims: new V2(1, vDoorHeight * 2) }
      };

      var exitIndicator = {
        top: { pos: new V2(roomDims.x / 2, 4), tile: tiles.floorArrowUp },
        bottom: { pos: new V2(roomDims.x / 2, roomDims.y - 3), tile: tiles.floorArrowDown },
        left: { pos: new V2(3, vDoorStart + (vDoorHeight / 2)), tile: tiles.floorArrowLeft },
        right: { pos: new V2(roomDims.x - 4, vDoorStart + (vDoorHeight / 2)), tile: tiles.floorArrowRight }
      };

      // Top door
      if (doors.top !== door.blocked) {
        collisionData.fill(new V2(hDoorStart * 2, 0), new V2(hDoorWidth * 2, 5), tiles.collide.none);
        collisionData.hline(collision.top.pos, collision.top.dims.x, tiles.collide.top);

        ceilingData.hline(new V2(8, 1), 6, tiles.tallDoorNUpperLeft, tiles.tallDoorNUpper, tiles.tallDoorNUpperRight);
        wallData.hline(new V2(8, 2), 6, tiles.tallDoorNLowerLeft, 0, tiles.tallDoorNLowerRight);
      }

      // Bottom door
      if (doors.bottom !== door.blocked) {
        collisionData.fill(new V2(18, (roomDims.y - 1) * 2), new V2(8, 5), tiles.collide.none);
        collisionData.hline(collision.bottom.pos, collision.bottom.dims.x, tiles.collide.bottom);

        ceilingData.hline(new V2(8, (roomDims.y - 2)), 6, tiles.tallDoorSLeft, tiles.tallDoorS, tiles.tallDoorSRight);
      }

      // Left door
      if (doors.left !== door.blocked) {
        collisionData.fill(new V2(0, vDoorStart * 2), new V2(5, vDoorHeight * 2), tiles.collide.none);
        collisionData.vline(collision.left.pos, collision.left.dims.y, tiles.collide.left);

        ceilingData.vline(new V2(1, 6), 4, tiles.tallDoorWTop, tiles.tallDoorW, tiles.tallDoorWUpperBottom);
        ceilingData.set(new V2(1, 10), tiles.tallDoorWLowerBottom);
      }

      // Right door
      if (doors.right !== door.blocked) {
        collisionData.fill(new V2((roomDims.x * 2) - 4, 15), new V2(5, 6), tiles.collide.none);
        collisionData.vline(collision.right.pos, collision.right.dims.y, tiles.collide.right);

        ceilingData.vline(new V2(roomDims.x - 2, 6), 4, tiles.tallDoorETop, tiles.tallDoorE, tiles.tallDoorEUpperBottom);
        ceilingData.set(new V2(roomDims.x - 2, 10), tiles.tallDoorELowerBottom);
      }

      var columns = {
        topLeft: { pos: new V2(7, 7), dir: new V2(-1, -1) },
        topRight: { pos: new V2(15, 7), dir: new V2(1, -1) },
        bottomLeft: { pos: new V2(7, 11), dir: new V2(-1, 1) },
        bottomRight: { pos: new V2(15, 11), dir: new V2(1, 1) }
      };

      var thickness = [3, 2].random();

      _(columns).each(function(column, direction) {
        if (Math.random() > 0.4) {

          var pos1 = column.pos;
          var pos2 = column.pos.add(column.dir.scale(thickness));

          var topLeft = new V2(Math.min(pos1.x, pos2.x), Math.min(pos1.y, pos2.y));
          var bottomRight = new V2(Math.max(pos1.x, pos2.x), Math.max(pos1.y, pos2.y));
          var topRight = new V2(bottomRight.x, topLeft.y);
          var bottomLeft = new V2(topLeft.x, bottomRight.y);

          var dims = bottomRight.sub(topLeft);

          collisionData.fill(topLeft.scale(2), dims.scale(2).add(new V2(0, 1)), tiles.collide.block);

          ceilingData.hline(topLeft.sub(new V2(0, 1)), dims.x, tiles.columnTopLeft, tiles.columnTop, tiles.columnTopRight);
          ceilingData.vline(topLeft, dims.y - 1, tiles.columnLeft);
          ceilingData.vline(topLeft.add(new V2(1, 0)), dims.y - 1, tiles.columnMiddle);
          ceilingData.vline(topRight.sub(new V2(1, 0)), dims.y - 1, tiles.columnRight);
          wallData.hline(bottomLeft, dims.x, tiles.columnLowerBottomLeft, tiles.columnLowerBottom, tiles.columnLowerBottomRight);
          wallData.hline(bottomLeft.sub(new V2(0, 1)), dims.x, tiles.columnUpperBottomLeft, tiles.columnUpperBottom, tiles.columnUpperBottomRight);
        }
      });

      var roomOriginInScreen = this.roomPosition.mult(this.roomDims).scale(16);
      _(['left', 'right', 'top', 'bottom']).each(function (direction) {

        if (doors[direction] === door.exit) {
          this.doorCollisionAreas.push(collision[direction]);

          var indicator = exitIndicator[direction];
          this.exitIndicatorsData.set(indicator.pos, indicator.tile);

          var exitPos = roomOriginInScreen.add(collision[direction].pos.scale(8));
          var exitDims = collision[direction].dims.scale(8);

          var destinationRoom = this.neighborPosition(direction);
          var destinationPos = destinationRoom.add(new V2(0.5, 0.5)).mult(this.roomDims).scale(16);

          ig.game.spawnEntity(EntityExit, exitPos.x, exitPos.y, {
            size: exitDims,
            destinationRoom: destinationRoom,
            destinationPos: destinationPos
          });
        }

        if (doors[direction] === door.exit || doors[direction] === door.entrance) {
          var spawnPos = roomOriginInScreen.add(spawnAreas[direction].pos.scale(16));
          var spawnDims = spawnAreas[direction].dims.scale(16);

          this.spawnAreas.push(ig.game.spawnEntity(EntitySpawn, spawnPos.x, spawnPos.y, {
            size: spawnDims,
            direction: direction
          }));
        }

      }, this);

      if (distance > 0.999) {
        var cornerX = [ 0.13, 0.86 ];
        var cornerY = [ 0.26, 0.8 ];
        var centerOfRoomInScreen = this.roomPosition.add(new V2(cornerX.random(), cornerY.random())).mult(this.roomDims).scale(16);
        ig.game.spawnEntity(EntityChalice, centerOfRoomInScreen.x, centerOfRoomInScreen.y);
      }

      collisionData.copyTo(layers.collisionData, roomPosition.mult(roomDims).scale(2));
      floorData.copyTo(layers.floorData, roomPosition.mult(roomDims));
      wallData.copyTo(layers.wallData, roomPosition.mult(roomDims));
      ceilingData.copyTo(layers.ceilingData, roomPosition.mult(roomDims));

      var spawnDoors = _(['left', 'right', 'top', 'bottom']).filter(function (direction) {
        return (doors[direction] === door.exit) || (doors[direction] === door.entrance);
      });

      this.waveDescription = generateWaves(ig.level, spawnDoors);
    },

    neighborPosition: function (direction) {
      var moveIn = {
        left: new V2(-1, 0),
        right: new V2(1, 0),
        top: new V2(0, -1),
        bottom: new V2(0, 1)
      };

      return this.roomPosition.add(moveIn[direction]);
    },

    unlock: function () {
      this.locked = false;
      var clearExitsData = new Array2d(this.roomDims.scale(2));

      _(this.doorCollisionAreas).each(function (area) {
        clearExitsData.fill(area.pos, area.dims, 1);
      }, this);

      clearExitsData.eraseIf(this.layers.collisionData, this.roomPosition.mult(this.roomDims).scale(2));

      var origin = this.roomPosition.mult(this.roomDims).scale(2);
      _(this.bumperAreas).each(function (bumperArea) {
         this.layers.collisionData.fill(bumperArea.pos.add(origin), bumperArea.dims, 0);
      }, this);

      this.exitIndicatorsData.replaceIf(this.layers.floorData, this.roomPosition.mult(this.roomDims));
    },

    lock: function () {
      this.locked = true;

      var origin = this.roomPosition.mult(this.roomDims).scale(2);
      _(this.bumperAreas).each(function (bumperArea) {
         this.layers.collisionData.fill(bumperArea.pos.add(origin), bumperArea.dims, 1);
      }, this);
    }
  });
});
