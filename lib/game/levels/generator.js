ig.module(
  'game.levels.generator'
)
.requires(
  'game.constants',
  'game.array2d',
  'game.v2'
)
.defines(function () {
  var adjacents = function (position) {
    var x = position.x;
    var y = position.y;
    return [
      { x: x, y: y - 1 },
      { x: x - 1, y: y },
      { x: x, y: y + 1 },
      { x: x + 1, y: y }
    ];
  };

  var drunkenPath = function (length, description) {
    var _drunkenPath = function (currentPath, totalLength) {
      if (currentPath.length === totalLength) {
        return currentPath;
      }

      var head = _(currentPath).last();
      var available = _(adjacents(head)).filter(function (position) {
        return !_(currentPath).findWhere(position);
      });
      available = _(available).shuffle();

      if (!available) {
        return currentPath;
      }

      for (var i = 0; i < available.length; i++) {
        currentPath.push(available[i]);
        var newPath = _drunkenPath(currentPath, totalLength);
        if (newPath.length === totalLength) {
          return currentPath;
        } else {
          currentPath.pop();
       }
      }
      return currentPath;
    };

    var path = _drunkenPath([{ x: 0, y : 0 }], length);

    return _(path).map(function (position, index) {
      return new Cell(position, index / (path.length - 1), description);
    })
  };

  var shortestPath = function (startCell, endCell, fromCell, toCell, world, description) {
    var matrix = _(world.cells.data).map(function (row) {
      return _(row).map(function (cell) {
        return (cell.distance !== Infinity) ? 1 : 0;
      });
    });

    var start = fromCell.position;
    var end = toCell.position;

    var occupiedGrid = new PF.Grid(world.size.x, world.size.y, matrix);
    var finder = new PF.AStarFinder();
    var path = finder.findPath(start.x, start.y, end.x, end.y, occupiedGrid);


    var interp = function (index) {
      var startDistance = startCell.distance;
      var endDistance = endCell.distance;
      var fraction = (index + 1) / (path.length + 1);

      return startDistance + fraction * (endDistance - startDistance);
    };

    return _(path).map(function (position, index) {
      return new Cell({ x: position[0], y: position[1] }, interp(index), description);
    });
  };

  var detourPaths = function (world, initialPathLength) {
    var paths = [];
    var openCells = _(world.openCells()).sortBy('distance');

    var attempts = 50;
    var maxPaths = Math.floor(initialPathLength / 1.5);

    // Try and create some alternate paths
    for (var attempt = 0; attempt < attempts && paths.length < maxPaths; attempt++) {
      var start = _(openCells).sample();
      var end = _(openCells).sample();

      var progress = end.distance - start.distance;

      if (progress > 0) {
        var first = _(world.openNeighbors(start)).sample();

        var last = _(world.openNeighbors(end)).sample();

        if (first && last) {
          var detourPath = shortestPath(start, end, first, last, world, "path" + paths.length);

          var detourProgress = detourPath.length / initialPathLength;
          if (detourProgress >= progress && (detourProgress < (progress * 2)) && (detourPath.length < 8)) {
            detourPath.push(end);
            detourPath.splice(0, 0, start);
            paths.push(detourPath);

            world.paintCells(detourPath);

            var labels = _(detourPath).map(function (cell) {
              return cell.position.x + ", " + cell.position.y + " (" + cell.distance + ")";
            });

            //console.log(labels.join(' -> '));
          }
        }
      }
    }
    return paths;
  };

  var Cell = function (position, distance, description) {
    this.position = position;
    this.distance = (distance !== undefined) ? distance : Infinity;
    this.description = description || '';

    this.doors = {
      top: door.blocked,
      bottom: door.blocked,
      left: door.blocked,
      right: door.blocked
    };
  };

  var World = ig.Class.extend({
    init: function (size) {
      this.size = size;
      this.cells = new Array2d(size);
      for (y = 0; y < size.y; y++) {
        for (x = 0; x < size.x; x++) {
          this.cells.set(new V2(x, y), new Cell({ x: x, y: y }));
        }
      }
      this.paths = [];
    },

    paintCells: function (cells) {
      var doors = {
        "-1": { 0: 'top' },
        "0": { "-1": 'left', 1: 'right' },
        "1": { 0: 'bottom' }
      };

      var lastCell = null;
      _(cells).each(function (cell) {
        var destinationCell = this.cells.get(new V2(cell.position.x, cell.position.y));

        if (destinationCell.distance === Infinity) {
          this.cells.set(new V2(cell.position.x, cell.position.y), cell);
          destinationCell = cell;
        }

        if (lastCell) {
          var dx = lastCell.position.x - cell.position.x;
          var dy = lastCell.position.y - cell.position.y;

          var entrance = doors[dy][dx];
          var exit = doors[-dy][-dx];

          lastCell.doors[exit] = door.exit;
          destinationCell.doors[entrance] = door.entrance;
        }

        lastCell = destinationCell;
      }, this);

      this.paths.push(cells);
    },

    openNeighbors: function (cell) {
      var open = [];
      var candidates = adjacents(cell.position);
      _(candidates).each(function (candidate) {
        if (candidate.x >= 0 && candidate.y >= 0 && candidate.x < this.size.x && candidate.y < this.size.y) {
          var adjacentCell = this.cells.get(new V2(candidate.x, candidate.y));
          if (adjacentCell.distance === Infinity) {
            open.push(adjacentCell);
          }
        }
      }, this);
      return open
    },

    openCells: function () {
      var open = [];
      for (y = 0; y < this.size.y; y++) {
        for (x = 0; x < this.size.x; x++) {
          var cell = this.cells.get(new V2(x, y));
          var neighbors = this.openNeighbors(cell);
          if (neighbors.length && cell.distance !== Infinity) {
            open.push(cell);
          }
        }
      }
      return open;
    }
  });

  CellGenerator = ig.Class.extend({
    init: function (levelLength) {
      var path = drunkenPath(levelLength, 'initial');
      var origin = {
        x: Math.min.apply(this, _(path).map(function (cell) { return cell.position.x; })) - 4,
        y: Math.min.apply(this, _(path).map(function (cell) { return cell.position.y; })) - 4
      };

      _(path).map(function (cell) {
        cell.position.x = cell.position.x - origin.x;
        cell.position.y = cell.position.y - origin.y;
      });

      this.extents = new V2(
        Math.max.apply(this, _(path).map(function (cell) { return cell.position.x; })) + 4,
        Math.max.apply(this, _(path).map(function (cell) { return cell.position.y; })) + 4
      );

      var world = new World(this.extents);
      world.paintCells(path);

      var allPaths = detourPaths(world, path.length);
      allPaths.push(path);

      this.paths = allPaths;
      this.cells = _(this.paths).flatten();
    }
  });
});

