ig.module(
  'game.levels.wavegenerator'
)
.requires(
  'game.entities.enemies'
)
.defines(function() {

  var r = function (c, n) {
    var result = [];
    for (var i = 0; i < n; i++) {
      result.push(c);
    }

    return result;
  };
  var s = function (listOfLists) {
    return _(listOfLists).flatten();
  };

  var enemyRounds = [
    [ // Level 0
      s([r(Goblin, 1)]),
    /*  s([r(Goblin, 2)]),
      s([r(Goblin, 5)]),
      s([r(Orc, 1)])*/
    ],
    [ // Level 1
      s([r(Goblin, 4)]),
      s([r(Goblin, 10)]),
      s([r(Goblin, 2), r(Orc, 1)]),
      s([r(Troll, 2)]),
      s([r(Wizard, 1)])
    ],
    [ // Level 2
      s([r(Goblin, 6)]),
      s([r(Goblin, 8), r(Orc, 2)]),
      s([r(Orc, 5)]),
      s([r(Troll, 2)]),
      s([r(Wizard, 1)]),
      s([r(Wizard, 2)]),
      s([r(Imp, 1)]),
      s([r(Squig, 1)])
    ],
    [ // Level 3
      s([r(Goblin, 15)]),
      s([r(Orc, 8)]),
      s([r(Troll, 3), r(Goblin, 5)]),
      s([r(Wizard, 2)]),
      s([r(Wizard, 1)]),
      s([r(Imp, 4)]),
      s([r(Squig, 1)])
    ],
    [ // Level 4
      s([r(Goblin, 15)]),
      s([r(Orc, 10)]),
      s([r(Troll, 6), r(Goblin, 5)]),
      s([r(Wizard, 2), r(Orc, 4)]),
      s([r(Wizard, 4)]),
      s([r(Imp, 5)]),
      s([r(Squig, 3)])
    ],
    [ // Level 5
      s([r(Goblin, 15), r(Orc, 5)]),
      s([r(Orc, 15), r(Troll, 2)]),
      s([r(Troll, 8), r(Goblin, 5)]),
      s([r(Wizard, 4), r(Orc, 7)]),
      s([r(Wizard, 5)]),
      s([r(Imp, 3)]),
      s([r(Squig, 4)])
    ],
    [ // Level 6
      s([r(Goblin, 15), r(Wizard, 1)]),
      s([r(Orc, 15), r(Squig, 2)]),
      s([r(Troll, 8), r(Goblin, 5)]),
      s([r(Wizard, 4), r(Imp, 3)]),
      s([r(Imp, 3), r(Goblin, 10)]),
      s([r(Squig, 5), r(Troll, 2)])
    ],
    [ // Level 7
      s([r(Goblin, 20), r(Wizard, 3)]),
      s([r(Orc, 20), r(Squig, 3)]),
      s([r(Troll, 8), r(Goblin, 10)]),
      s([r(Wizard, 6), r(Imp, 4)]),
      s([r(Imp, 4), r(Goblin, 20)]),
      s([r(Squig, 10), r(Troll, 2)])
    ],
    [ // Level 8
      s([r(Goblin, 20), r(Wizard, 5)]),
      s([r(Orc, 20), r(Squig, 5)]),
      s([r(Troll, 10), r(Goblin, 10)]),
      s([r(Wizard, 8), r(Imp, 4)]),
      s([r(Imp, 6), r(Goblin, 20)]),
      s([r(Squig, 15), r(Troll, 2)])
    ],
    [ // Level 9
      s([r(Goblin, 20), r(Wizard, 10)]),
      s([r(Orc, 20), r(Squig, 10)]),
      s([r(Troll, 10), r(Goblin, 10)]),
      s([r(Wizard, 15), r(Imp, 4)]),
      s([r(Imp, 10), r(Goblin, 20)]),
      s([r(Squig, 20), r(Troll, 10)])
    ]
  ];

  generateWaves = function (level, spawnDoors) {
    var hardest = Math.min(9, level - 1);
    var middle = Math.min(9, Math.floor(hardest / 2));
    var easiest = Math.min(9, Math.floor(hardest / 4));

    var numberOfWaves = [2, 2, 3, 3, 4, 4, 4, 5, 5, 5, 5];
    var numberOfRounds = [2, 3, 4, 4, 5, 5, 6, 6, 6, 6, 7];
    var timeBetweenRounds = [8, 8, 6, 6, 5, 5, 5, 4, 4, 3];
    var roundIndex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    var difficulty = _([hardest, middle, middle, easiest]).shuffle();

    var waves = [];

    for (var wave = 0; wave < numberOfWaves[difficulty[0]]; wave++) {
      var rounds = [];
      var arrival = 0;
      for (var round = 0; round < numberOfRounds[difficulty[1]]; round++) {
        var door1 = spawnDoors.random();
        var door2 = spawnDoors.random();

        var enemyDifficulty = roundIndex[hardest];
        if (door1 !== door2) {
          enemyDifficulty = Math.max(enemyDifficulty - 1, 0);
          rounds.push({ after: arrival, location: door1, enemies: enemyRounds[enemyDifficulty].random() });
          rounds.push({ after: arrival, location: door2, enemies: enemyRounds[enemyDifficulty].random() });
        } else {
          rounds.push({ after: arrival, location: door1, enemies: enemyRounds[enemyDifficulty].random() });
        }
        arrival += timeBetweenRounds[difficulty[2]];
      }

      waves.push(rounds);
    }

    return waves;
  };
});

