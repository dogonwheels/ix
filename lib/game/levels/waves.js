ig.module(
  'game.levels.waves'
)
.requires(
  'impact.impact'
)
.defines(function () {
  Waves = ig.Class.extend({
    init: function (waveDescriptions, spawnAreas) {
      this.waveTime = new ig.Timer();
      this.waves = waveDescriptions.slice(0); // Take a copy of the wave descriptions
      this.spawnAreas = spawnAreas;

      // Start the waves for the room
      this.finished = false;
      this.currentWave = [];
    },
    update: function () {
      ig.game.entities = _(ig.game.entities).sortBy(function (e) { return e.pos.y; });

      var spawnsPresent = _(ig.game.entities).filter(function (e) {
        return e.spawned;
      }).length;
      if (this.waves.length === 0 && this.currentWave.length === 0 && !spawnsPresent) {
        // If no spawned entities left then can move to the next room. Finished this set of waves
        this.finished = true;
      } else {
        if (this.currentWave.length === 0 && !spawnsPresent) {
          // Slice off the current wave from the rest and start the wave
          this.currentWave = _(this.waves).head();
          this.waves = _(this.waves).tail();
          this.waveTime.reset();
        }

        // Split the wave's spawns into those that are overdue and those that are pending
        var currentTime = this.waveTime.delta();
        var toSpawn = _(this.currentWave).filter(function (spawn) {
          return spawn.after < currentTime;
        });
        this.currentWave = _(this.currentWave).difference(toSpawn);

        var spawnAreas = this.spawnAreas;
        _(toSpawn).each(function (spawn) {
          var area = _(spawnAreas).find(function (spawnArea) {
            return _(spawnArea.direction).isEqual(spawn.location);
          });

          _(spawn.enemies).each(function (Entity) {
            var entitySize = Entity.prototype.size;
            var x = (Math.random() * (area.size.x - entitySize.x)) + area.pos.x;
            var y = (Math.random() * (area.size.y - entitySize.y)) + area.pos.y;
            ig.game.spawnEntity(Entity, x, y);
          });
        });
      }
    },
    respawnedPlayer: function () {
      var spawnsPresent = _(ig.game.entities).filter(function (e) {
        return e.spawned;
      });
      var player = ig.game.getEntitiesByType(EntityPlayer)[0];
      if (player === undefined) {
          console.error("No EntityPlayer found");
      }
      _(spawnsPresent).each(function (creature) {
        creature.player = player;
      });
    },

    finished: false
  });
});