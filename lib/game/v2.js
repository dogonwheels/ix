ig.module(
  'game.v2'
)
.requires(
)
.defines(function() {
  V2 = ig.Class.extend({
    init: function (x, y) {
      if (x.x && x.y) {
        this.x = x.x;
        this.y = x.y;
      } else {
        this.x = x;
        this.y = y;
      }
    },

    add: function (that) {
      return new V2(this.x + that.x, this.y + that.y);
    },

    sub: function (that) {
      return new V2(this.x - that.x, this.y - that.y);
    },

    scale: function (amount) {
      return new V2(this.x * amount, this.y * amount);
    },

    mult: function (that) {
      return new V2(this.x * that.x, this.y * that.y);
    }
  });
});