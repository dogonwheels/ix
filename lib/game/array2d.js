ig.module(
  'game.array2d'
)
.requires(
  'game.v2'
)
.defines(function() {
  Array2d = ig.Class.extend({
    init: function (dims, fill) {
      this.dims = dims;
      this.data = [];

      for (var y = 0; y < dims.y; y++) {
        var row = [];
        for (var x = 0; x < dims.x; x++) {
          row.push(fill);
        }
        this.data.push(row);
      }
    },

    copyTo: function (target, dPos) {
      for (var y = 0; y < this.dims.y; y++) {
        for (var x = 0; x < this.dims.x; x++) {
          var pos = new V2(x, y);
          target.set(dPos.add(pos), this.get(pos));
        }
      }
    },

    eraseIf: function (target, dPos) {
      for (var y = 0; y < this.dims.y; y++) {
        for (var x = 0; x < this.dims.x; x++) {
          var pos = new V2(x, y);
          if (this.get(pos)) {
            target.set(dPos.add(pos), 0);
          }
        }
      }
    },

    replaceIf: function (target, dPos) {
      for (var y = 0; y < this.dims.y; y++) {
        for (var x = 0; x < this.dims.x; x++) {
          var pos = new V2(x, y);
          if (this.get(pos)) {
            target.set(dPos.add(pos), this.get(pos));
          }
        }
      }
    },

    set: function (pos, value) {
      if (pos.x >= 0 && pos.x < this.dims.x && pos.y >= 0 && pos.y < this.dims.y) {
        this.data[pos.y][pos.x] = value;
      }
    },

    get: function (pos) {
      if (pos.x >= 0 && pos.x < this.dims.x && pos.y >= 0 && pos.y < this.dims.y) {
        return this.data[pos.y][pos.x];
      } else {
        console.log('tried to access ', pos.x, pos.y, ' of a ', this.dims.x, this.dims.y, ' array');
      }
    },

    hline: function (pos, l, s, m, e) {
      var m = m === undefined ? s : m;
      var e = e === undefined ? s : e;

      this.set(pos, s);
      for (var px = 1; px < (l - 1); px++) {
        this.set(pos.add(new V2(px, 0)), m);
      }
      this.set(pos.add(new V2(l - 1, 0)), e);
    },

    vline: function (pos, l, s, m, e) {
      var m = m === undefined ? s : m;
      var e = e === undefined ? s : e;

      this.set(pos, s);
      for (var py = 1; py < (l - 1); py++) {
        this.set(pos.add(new V2(0, py)), m);
      }
      this.set(pos.add(new V2(0, l - 1)), e);
    },

    fill: function (pos, dims, fill) {
      for (var dy = 0; dy < dims.y; dy++) {
        for (var dx = 0; dx < dims.x; dx++) {
          this.set(pos.add(new V2(dx, dy)), fill);
        }
      }
    },

    stroke: function (pos, dims, fill) {
      for (var dy = 0; dy < dims.y; dy++) {
        for (var dx = 0; dx < dims.x; dx++) {
          var newPos = pos.add(new V2(dx, dy));
          if (newPos.x == pos.x || newPos.y == pos.y ||
              newPos.x == pos.x + (dims.x - 1) || newPos.y == pos.y + (dims.y - 1)) {
            this.set(newPos, fill);
          }
        }
      }
    }
  });
});