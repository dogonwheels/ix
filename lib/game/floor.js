ig.module(
  'game.floor'
)
.requires(
  'impact.impact',
  'game.levels.waves',
  'game.entities.blackpotion',
  'game.entities.potions'
)
.defines(function () {
  Floor = ig.Class.extend({
    init: function (rooms, startingRoomPos, roomSize) {
      this.rooms = rooms;
      this.currentPos = startingRoomPos;
      this.room = this.rooms.get(this.currentPos);
      this.roomSize = roomSize;
      this.potionDrop = new ig.Timer(4);
    },

    start: function () {
      var playerSpawn = this.currentPos.add(new V2(0.5, 0.5)).mult(this.roomSize).scale(16);
      ig.game.spawnEntity(EntityPlayer, playerSpawn.x, playerSpawn.y, { floor: this });

      this.startRoom();
    },

    respawn: function () {
      var playerSpawn = this.currentPos.add(new V2(0.5, 0.5)).mult(this.roomSize).scale(16);
      ig.game.spawnEntity(EntityPlayer, playerSpawn.x, playerSpawn.y, { floor: this });

      this.waves.respawnedPlayer();
    },

    startRoom: function () {
      console.log("Starting in room ", this.currentPos);

      this.room.lock();
      this.waves = new Waves(this.room.waveDescription, this.room.spawnAreas);
    },

    update: function () {
      if (this.waves.finished) {
        this.unlockRoom();
      } else {
        if (this.potionDrop.delta() > 0) {
          var potion = [ RedPotion, GreenPotion, BluePotion, BlackPotion ].random();
          var spawnPoint = this.room.weaponSpawnPositions.random();
          var pos = this.currentPos.mult(this.roomSize).add(spawnPoint).scale(16);
          ig.game.spawnEntity(potion, pos.x, pos.y);

          this.potionDrop.set(4 + Math.random() * 10);
        }

        this.waves.update();
      }
    },

    arrivedAt: function (roomPos) {
      this.currentPos = roomPos;
      this.room = this.rooms.get(this.currentPos);
      this.startRoom();
    },

    unlockRoom: function () {
      this.room.unlock();
    }
  });
});