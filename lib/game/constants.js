ig.module(
  'game.constants'
)
.requires(
)
.defines(function () {
  tiles = {
    floorBlue: 1,
    floorArrowDown: 11,
    floorArrowUp: 12,
    floorArrowLeft: 21,
    floorArrowRight: 22,

    ceiling: 31,

    tallWallNUpperAlt: 8,
    tallWallNLowerAlt: 18,

    tallWallNUpper: 9,
    tallWallNWUpper: 3,
    tallWallNEUpper: 7,

    tallWallNLower: 19,
    tallWallNWLower: 13,
    tallWallNELower: 17,

    tallWallW: 23,
    tallWallE: 27,

    tallWallSW: 73,
    tallWallS: 78,
    tallWallSE: 77,

    tallDoorNUpperLeft: 4,
    tallDoorNUpper: 5,
    tallDoorNUpperRight: 6,

    tallDoorSLeft: 74,
    tallDoorS: 75,
    tallDoorSRight: 76,

    tallDoorWTop: 33,
    tallDoorW: 43,
    tallDoorWUpperBottom: 53,
    tallDoorWLowerBottom: 63,

    tallDoorETop: 37,
    tallDoorE: 47,
    tallDoorEUpperBottom: 57,
    tallDoorELowerBottom: 67,

    tallDoorNLowerLeft: 14,
    tallDoorNLowerRight: 16,

    columnUpperBottomLeft: 54,
    columnUpperBottom: 55,
    columnUpperBottomRight: 56,

    columnLowerBottomLeft: 64,
    columnLowerBottom: 65,
    columnLowerBottomRight: 66,

    columnTopLeft: 34,
    columnTop: 35,
    columnTopRight: 36,

    columnLeft: 44,
    columnMiddle: 45,
    columnRight: 46,

    collide: {
      none: 0,
      block: 1,
      bottom: 12,
      top: 23,
      left: 34,
      right: 45
    }
  };

  door = {
    exit: 1,
    entrance: 2,
    blocked: 3
  };
});
