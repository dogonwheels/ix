ig.module(
  'game.entities.logo'
).requires(
  'impact.entity'
).defines(function () {
  EntityLogo = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/logo.png', 32, 39),
    init: function (x, y, settings) {
      this.parent(x, y, settings);
      this.addAnim('idle', 0.1, [0, 1, 2, 4, 5, 6, 7, 8], true);
    }
  });
});