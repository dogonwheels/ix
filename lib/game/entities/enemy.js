ig.module(
    'game.entities.enemy'
).requires(
    'impact.entity',
    'game.geometry',
    'game.entities.gold',
    'game.entities.particles',
    'game.entities.enemyfireball'
).defines(function() {
  Enemy = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/enemies.png', 16, 16),
    type: ig.Entity.TYPE.B,
    checkAgainst: ig.Entity.TYPE.A,
    collides: ig.Entity.COLLIDES.PASSIVE,
    action: "chase",
    spawned: true,
    init: function(x, y, settings) {
      this.choiceTimer = new ig.Timer();
      this.parent(x, y, settings);
      this.addAnim('attack', 1, [this.cell + 1]);
      this.addAnim('idle', 0.1, [this.cell + 2, this.cell, this.cell + 3, this.cell]);
      this.player = ig.game.getEntitiesByType(EntityPlayer)[0];
      if (this.player === undefined) {
          console.error("No EntityPlayer found");
      }
    },
    check: function(other) {
      other.receiveDamage(1, this);
    },
    kill: function() {
      var coins = Math.floor(this.gold / 5);
      var noise = [-4, -3, -2, -1, 0, 1, 2, 3, 4];
      for (var i = 0; i < coins; i++) {
        ig.game.spawnEntity(Gold, this.pos.x + noise.random(), this.pos.y + 3 + noise.random(), { gold: 5 });
      }
      ig.game.spawnEntity(Wound, this.pos.x, this.pos.y + 3, { sx: 0, sy: 0 });
      this.parent();
    },
    update: function () {
      if (this.choiceTimer.delta() > (Math.random() / 3) + 0.2) {
        var dice = Math.random();
        if (dice > this.attack + this.potter) {
          this.action = "chase";
          this.currentAnim = this.anims.idle;
        } else if (dice > this.attack) {
          this.action = "potter";
          this.potterDirection = { x: (Math.random() - 0.5) * 10, y: (Math.random() - 0.5) * 10 };
          this.currentAnim = this.anims.idle;
        } else {
          this.action = "attack";
          if (this.canShoot && this.player) {
            var dir = directionTo(this.pos, this.player.pos);
            ig.game.spawnEntity(EnemyFireBall, this.pos.x, this.pos.y + 5, {sx: dir.x, sy: dir.y});
          }
          if (this.canExplode) {
            ig.game.spawnEntity(EnemyFireBall, this.pos.x, this.pos.y + 5, {sx: -1, sy: -1});
            ig.game.spawnEntity(EnemyFireBall, this.pos.x, this.pos.y + 5, {sx: -1, sy: 1});
            ig.game.spawnEntity(EnemyFireBall, this.pos.x, this.pos.y + 5, {sx: 1, sy: -1});
            ig.game.spawnEntity(EnemyFireBall, this.pos.x, this.pos.y + 5, {sx: 1, sy: 1});
          }
          this.currentAnim = this.anims.attack;
        }
        this.choiceTimer.reset();
      }
      var speed = this.speed;
      var move = { x: 0, y : 0 };

      switch (this.action) {
        case "chase":
          if (this.player) {
            move = moveTowards(new V2(this.pos), new V2(this.player.pos));
          }
          break;
        case "potter":
          move = moveInDirection(new V2(this.potterDirection));
          break;
      }

      this.vel.x = speed * move.x;
      this.vel.y = speed * move.y;

      this.parent();
    }
  });
});