ig.module(
  'game.entities.potion'
)
.requires(
  'impact.entity'
)
.defines(function () {
  Potion = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/entities.png', 16, 16),
    offset: {x: 5, y: 5},
    size: {x: 5, y: 8},
    type: ig.Entity.TYPE.NONE,
    checkAgainst: ig.Entity.TYPE.A,
    collides: ig.Entity.COLLIDES.NEVER,
    init: function (x, y, settings) {
      this.parent(x, y, settings);
      this.addAnim('idle', 1, [this.cell]);
      this.life = new ig.Timer(this.lifetime);
    },
    update: function () {
      if (this.life.delta() > 0) {
        this.kill();
      }
      this.parent();
    },
    check: function (other) {
      other.weapon = new this.weapon();
      this.kill();
    }
  });
});