ig.module(
  'game.entities.gold'
).requires(
  'impact.entity'
).defines(function() {
  Gold = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/entities.png', 16, 16),
    size: {x: 6, y: 8},
    offset: {x: 4, y: 4},
    type: ig.Entity.TYPE.B,
    checkAgainst: ig.Entity.TYPE.A,
    collides: ig.Entity.COLLIDES.PASSIVE,
    passthru: true,
    init: function(x, y, settings) {
      this.parent(x, y, settings);
      this.addAnim('idle', 1, [5]);
      this.life = new ig.Timer(4 + Math.random());
    },
    update: function () {
      if (this.life.delta() > 0) {
        this.kill();
      }
      this.parent();
    },
    check: function() {
      ig.gold += this.gold;
      this.kill();
    }
  });
});