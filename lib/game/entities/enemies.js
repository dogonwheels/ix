ig.module(
  'game.entities.enemies'
).requires(
  'impact.entity',
  'game.entities.enemy'
).defines(function() {
  var rowWidth = 5;
  Goblin = Enemy.extend({
    cell: 0,
    size: {x: 7, y: 10},
    offset: {x: 5, y: 3},
    health: 1,
    speed: 50,
    attack: 0.1,
    potter: 0.2,
    gold: 5
  });

  Orc = Enemy.extend({
    cell: rowWidth,
    size: {x: 8, y: 11},
    offset: {x: 4, y: 3},
    health: 2,
    speed: 55,
    attack: 0.05,
    potter: 0.05,
    gold: 10
  });

  Troll = Enemy.extend({
    cell: rowWidth * 2,
    size: {x: 8, y: 11},
    offset: {x: 4, y: 3},
    health: 5,
    speed: 30,
    attack: 0.05,
    potter: 0.1,
    gold: 15
  });

  Wizard = Enemy.extend({
    cell: rowWidth * 3,
    canShoot: true,
    size: {x: 9, y: 11},
    offset: {x: 3, y: 2},
    health: 2,
    speed: 65,
    attack: 0.1,
    potter: 0.3,
    gold: 30
  });

  Imp = Enemy.extend({
    cell: rowWidth * 4,
    canShoot: true,
    size: {x: 6, y: 7},
    offset: {x: 5, y: 6},
    health: 1,
    speed: 80,
    attack: 0.2,
    potter: 0.5,
    gold: 25
  });

  Squig = Enemy.extend({
    cell: rowWidth * 5,
    canExplode: true,
    size: {x: 9, y: 10},
    offset: {x: 4, y: 3},
    health: 4,
    speed: 40,
    attack: 0.05,
    potter: 0.5,
    gold: 35
  });
});