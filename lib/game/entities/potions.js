ig.module(
  'game.entities.potions'
)
.requires(
  'impact.entity',
  'game.entities.potion',
  'game.weapons.fastfireballs',
  'game.weapons.multifireballs',
  'game.weapons.strongfireballs'
)
.defines(function () {
  RedPotion = Potion.extend({
    cell: 1,
    lifetime: 6,
    weapon: FastFireBalls
  });

  BluePotion = Potion.extend({
    cell: 2,
    lifetime: 6,
    weapon: StrongFireBalls
  });

  GreenPotion = Potion.extend({
    cell: 3,
    lifetime: 4,
    weapon: MultiFireBalls
  })
});