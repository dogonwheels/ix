ig.module(
  'game.entities.timebomb'
)
.requires(
  'impact.entity'
)
.defines(function () {
  TimeBomb = ig.Entity.extend({
    init: function (x, y, settings) {
      this.detonateIn = new ig.Timer(settings.distance / 120);
      this.target = settings.target;
    },
    update: function () {
      if (this.detonateIn.delta() > 0) {
        this.target.kill();
        this.kill();
      }
      this.parent();
    }
  });
});