ig.module(
  'game.entities.strongfireball'
)
.requires(
  'impact.entity'
)
.defines(function () {
  StrongFireBall = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/player.png', 16, 16),
    offset: {x: 7, y: 8},
    size: {x: 2, y: 2},
    maxVel: {x: 200, y: 200},
    checkAgainst: ig.Entity.TYPE.B,
    collides: ig.Entity.COLLIDES.PASSIVE,
    init: function (x, y, settings) {
      this.parent(x, y, settings);
      this.addAnim('idle', 0.2, [10, 11, 12]);
      this.sx = settings.sx;
      this.sy = settings.sy;
      this.vel.x = settings.sx * 200;
      this.vel.y = settings.sy * 200;
    },
    update: function () {
      if (this.vel.x === 0 && this.vel.y === 0) {
        this.kill();
      }
      this.parent();
    },
    handleMovementTrace: function (res) {
      if (res.collision.x || res.collision.y) {
        this.kill();
      } else {
        this.parent(res);
      }
    },
    check: function (other) {
      if (other.spawned) {
        other.receiveDamage(3, this);
        var d = Math.max(Math.abs(this.vel.x), Math.abs(this.vel.y)) + 0.01;
        var sx = this.vel.x / d;
        var sy = this.vel.y / d;
        ig.game.spawnEntity(Wound, this.pos.x, this.pos.y, { sx: sx, sy: sy });
      }
      if (!other.passthru) {
        this.kill();
      }
    }
  });
});