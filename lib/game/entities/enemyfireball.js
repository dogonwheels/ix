ig.module(
  'game.entities.enemyfireball'
)
.requires(
  'impact.entity'
)
.defines(function () {
  EnemyFireBall = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/player.png', 16, 16),
    offset: {x: 7, y: 8},
    size: {x: 2, y: 2},
    maxVel: {x: 200, y: 200},
    checkAgainst: ig.Entity.TYPE.A,
    collides: ig.Entity.COLLIDES.PASSIVE,
    init: function (x, y, settings) {
      this.parent(x, y, settings);
      this.addAnim('idle', 0.2, [5, 6, 7]);
      this.sx = settings.sx;
      this.sy = settings.sy;
      this.vel.x = settings.sx * 200;
      this.vel.y = settings.sy * 200;
    },
    handleMovementTrace: function (res) {
      if (res.collision.x || res.collision.y) {
        this.kill();
      } else {
        this.parent(res);
      }
    },
    update: function () {
      if (this.vel.x === 0 && this.vel.y === 0) {
        this.kill();
      }
      this.parent();
    },
    check: function (other) {
      if (!other.passthru) {
        other.receiveDamage(1, this);
        this.kill();
      }
    }
  });
});