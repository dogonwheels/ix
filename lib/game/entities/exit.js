ig.module(
  'game.entities.exit'
)
.requires(
  'impact.entity'
)
.defines(function() {
  EntityExit = ig.Entity.extend({
    checkAgainst: ig.Entity.TYPE.A,
    collides: ig.Entity.COLLIDES.PASSIVE,

    check: function(other) {
      if (other instanceof EntityPlayer && !other.autopilot) {
        console.log('triggered exit to ', this.destinationRoom);
        other.autopilot = true;
        other.destinationPos = this.destinationPos;
        other.destinationRoom = this.destinationRoom;
      }
      this.parent(other);
    },

    draw: function () {
      /*ig.system.context.fillStyle = 'rgba(0, 128, 0, 0.7)';
			ig.system.context.fillRect(
				ig.system.getDrawPos(this.pos.x - ig.game.screen.x),
				ig.system.getDrawPos(this.pos.y - ig.game.screen.y),
				this.size.x * ig.system.scale,
				this.size.y * ig.system.scale
			);*/
    }
  });
});