ig.module(
  'game.entities.particles'
).requires(
  'impact.entity'
).defines(function () {
  Particle = ig.Entity.extend({
    life: 0.2,
    type: ig.Entity.TYPE.NONE,
    checkAgainst: ig.Entity.TYPE.B,
    collides: ig.Entity.COLLIDES.PASSIVE,
    animSheet: new ig.AnimationSheet('media/particles.png', 16, 16),
    init: function (x, y, settings) {
      this.parent(x, y, settings);
      this.vel.x = (settings.dx) * 85;
      this.vel.y = (settings.dy) * 85;
      this.life *= Math.random() + 1;
      this.age = new ig.Timer();
      var cell = Math.floor(Math.random() * 25);
      this.addAnim("idle", 0.2, [cell]);
    },
    update: function () {
      if (this.age.delta() > this.life) {
        this.kill();
      } else {
        this.parent();
      }
    }
  });

  Wound = ig.Entity.extend({
    particles: 15,
    type: ig.Entity.TYPE.NONE,
    checkAgainst: ig.Entity.TYPE.NONE,
    collides: ig.Entity.COLLIDES.PASSIVE,
    life: new ig.Timer(2),
    init: function (x, y, settings) {
      this.parent(x, y, settings);
      if (!settings.sx) settings.sx = 0;
      if (!settings.sy) settings.sy = 0;

      for (var index = 0; index < this.particles; index++) {
        var dx = settings.sx + (Math.random() - 0.5);
        var dy = settings.sy + (Math.random() - 0.5);
        ig.game.spawnEntity(Particle, x, y, {dx: dx, dy: dy});
      }
      if (settings.onFinished) {
        this.onFinished = settings.onFinished;
      } else {
        this.onFinished = function () {};
      }
      this.life.reset();
    },
    update: function () {
      if (this.life.delta() > 0) {
        this.onFinished();
        this.kill();
      }
    }
  });
});