ig.module(
  'game.entities.player'
)
.requires(
  'impact.entity',
  'game.weapons.fireballs'
)
.defines(function() {
  EntityPlayer = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/player.png', 16, 16),
    size: {x: 7, y: 10},
    offset: {x: 5, y: 3},
    type: ig.Entity.TYPE.A,
    checkAgainst: ig.Entity.TYPE.NONE,
    collides: ig.Entity.COLLIDES.PASSIVE,
    autopilot: false,
    health: 1,
    invincible: new ig.Timer(2),
    init: function (x, y, settings) {
      this.parent(x, y, settings);
      this.addAnim('idle', 1, [0]);
      this.addAnim('walk', 0.1, [0, 15, 0, 16]);
      this.addAnim('invincible', 0.1, [0, 1]);

      this.weapon = new FireBalls();

      this.currentAnim = this.anims.invincible;
      this.invincible.reset();
    },

    update: function () {
      var delta = new V2(0, 0);
      var speed = 70;

      if (this.invincible.delta() > 0 && this.currentAnim === this.anims.invincible) {
        this.currentAnim = this.anims.idle;
      }

      if (this.autopilot) {
        // Move closer to the destination - maximum speed in each direction
        var move = moveTowards(this.pos, this.destinationPos);

        if (move.x === 0 && move.y === 0) {
          this.floor.arrivedAt(this.destinationRoom);
          this.autopilot = false;
        } else {
          delta = move.scale(speed);
        }
      } else {
        if (ig.input.state('left')) delta = delta.sub(new V2(speed, 0));
        if (ig.input.state('right')) delta = delta.add(new V2(speed, 0));
        if (ig.input.state('up')) delta = delta.sub(new V2(0, speed));
        if (ig.input.state('down')) delta = delta.add(new V2(0, speed));

        if (this.currentAnim !== this.anims.invincible) {
          if (delta.x !== 0 || delta.y !== 0) {
            if (this.currentAnim !== this.anims.walk) {
              this.currentAnim = this.anims.walk;
            }
          } else {
            if (this.currentAnim !== this.anims.idle) {
              this.currentAnim = this.anims.idle;
            }
          }
        }

        var sx = 0;
        var sy = 0;
        if (ig.input.state('fireleft')) {
          sx = -1;
        } else if (ig.input.state('fireright')) {
          sx = 1;
        }

        if (ig.input.state('fireup')) {
          sy = -1;
        } else if (ig.input.state('firedown')) {
          sy = 1;
        }

        if (sx !== 0 || sy !== 0) {
          this.weapon.fire(this.pos, { x: sx, y: sy });
          if (this.weapon.depleted) {
            this.weapon = new FireBalls();
          }
        }

        if (this.weapon.usesMana) {
          ig.game.ammo = this.weapon.ammo;
        } else {
          ig.game.ammo = 0;
        }
      }

      this.vel = delta;

      this.parent();
    },
    kill: function() {
      if (this.invincible.delta() > 0) {
        this.parent();
        ig.game.spawnEntity(Wound, this.pos.x, this.pos.y);
        ig.game.died();
      }
    }
  });
});