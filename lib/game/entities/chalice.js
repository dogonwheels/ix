ig.module(
  'game.entities.chalice'
).requires(
  'impact.entity'
).defines(function() {
  EntityChalice = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/entities.png', 16, 16),
    size: {x: 5, y: 10},
    offset: {x: 5, y: 3},
    type: ig.Entity.TYPE.B,
    checkAgainst: ig.Entity.TYPE.A,
    collides: ig.Entity.COLLIDES.PASSIVE,
    init: function(x, y, settings) {
      this.parent(x, y, settings);
      this.addAnim('idle', 1, [0]);
    },
    check: function() {
       ig.completedFloor();
    }
  });
});