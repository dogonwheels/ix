ig.module(
  'game.entities.blackpotion'
)
.requires(
  'impact.entity',
  'game.entities.player',
  'game.entities.timebomb'
)
.defines(function () {
  BlackPotion = ig.Entity.extend({
    animSheet: new ig.AnimationSheet('media/entities.png', 16, 16),
    offset: {x: 5, y: 5},
    size: {x: 5, y: 8},
    type: ig.Entity.TYPE.NONE,
    checkAgainst: ig.Entity.TYPE.A,
    collides: ig.Entity.COLLIDES.NEVER,
    cell: 4,
    lifetime: 3,
    init: function (x, y, settings) {
      this.parent(x, y, settings);
      this.addAnim('idle', 1, [this.cell]);
      this.life = new ig.Timer(this.lifetime);
    },
    update: function () {
      if (this.life.delta() > 0) {
        this.kill();
      }
      this.parent();
    },
    check: function (other) {
      if (other instanceof EntityPlayer) {
        var creatures = _(ig.game.entities).filter(function (e) { return e.spawned; });

        _(creatures).each(function (creature) {
          var dx = other.pos.x - creature.pos.x;
          var dy = other.pos.y - creature.pos.y;

          var distance = Math.sqrt((dx * dx) + (dy * dy));
          ig.game.spawnEntity(TimeBomb, creature.pos.x, creature.pos.y, { distance: distance, target: creature });
        });

        this.kill();
      }
    }
  });
});