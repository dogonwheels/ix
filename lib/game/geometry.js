ig.module(
  'game.geometry'
)
.requires(
)
.defines(function () {
  moveInDirection = function (direction) {
    var dx = 0;
    var dy = 0;
    var tx = direction.x / direction.y;
    var ty = direction.y / direction.x;

    if (tx > 1) {
      dx = 1;
    } else if (tx < -1) {
      dx = -1;
    }
    if (ty > 1) {
      dy = 1;
    } else if (ty < -1) {
      dy = -1;
    }

    return { x: dx, y: dy };

  };
  moveTowards = function (fromPosition, toPosition) {
    var delta = toPosition.sub(fromPosition);

    var dx = 0;
    var dy = 0;
    if (Math.abs(delta.x) > Math.abs(delta.y)) {
      if (delta.x > 1) {
        dx = 1;
      } else if (delta.x < -1) {
        dx = -1;
      }
    } else {
      if (delta.y > 1) {
        dy = 1;
      } else if (delta.y < -1) {
        dy = -1;
      }
    }

    return new V2(dx, dy);
  };
  directionTo = function (fromPosition, toPosition) {
    var dx = toPosition.x - fromPosition.x;
    var dy = toPosition.y - fromPosition.y;
    var mag = Math.sqrt((dx * dx) + (dy * dy));

    return { x: dx / mag, y: dy / mag };
  };
});