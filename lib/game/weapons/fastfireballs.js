ig.module(
  'game.weapons.fastfireballs'
)
.requires(
  'impact.impact',
  'game.entities.fireball',
  'game.weapons.weapon'
)
.defines(function () {
  FastFireBalls = Weapon.extend({
    fire: function (pos, direction) {
      if (this.coolOff.delta() > 0) {
        ig.game.spawnEntity(FireBall, pos.x, pos.y + 5, {sx: direction.x, sy: direction.y});
        this.coolOff.set(0.15);
        this.ammo -= 10;
      }
      this.parent();
    }
  });
});