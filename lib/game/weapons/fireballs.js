ig.module(
  'game.weapons.fireballs'
)
.requires(
  'impact.impact',
  'game.entities.fireball',
  'game.weapons.weapon'
)
.defines(function () {
  FireBalls = Weapon.extend({
    usesMana: false,
    fire: function (pos, direction) {
      if (this.coolOff.delta() > 0) {
        ig.game.spawnEntity(FireBall, pos.x, pos.y + 5, {color: 0, sx: direction.x, sy: direction.y});
        this.coolOff.set(0.3);
      }
    }
  });
});