ig.module(
  'game.weapons.multifireballs'
)
.requires(
  'impact.impact',
  'game.entities.fireball',
  'game.weapons.weapon'
)
.defines(function () {
  MultiFireBalls = Weapon.extend({
    fire: function (pos, direction) {
      if (this.coolOff.delta() > 0) {

        ig.game.spawnEntity(FireBall, pos.x, pos.y + 5, {sx: 1, sy: 0});
        ig.game.spawnEntity(FireBall, pos.x, pos.y + 5, {sx: 0, sy: 1});
        ig.game.spawnEntity(FireBall, pos.x, pos.y + 5, {sx: -1, sy: 0});
        ig.game.spawnEntity(FireBall, pos.x, pos.y + 5, {sx: 0, sy: -1});
        this.coolOff.set(0.3);
        this.ammo -= 5;
      }
      this.parent();
    }
  });
});