ig.module(
  'game.weapons.strongfireballs'
)
.requires(
  'impact.impact',
  'game.entities.strongfireball',
  'game.weapons.weapon'
)
.defines(function () {
  StrongFireBalls = Weapon.extend({
    fire: function (pos, direction) {
      if (this.coolOff.delta() > 0) {
        ig.game.spawnEntity(StrongFireBall, pos.x, pos.y + 5, {sx: direction.x, sy: direction.y});
        this.coolOff.set(0.3);
        this.ammo -= 5;
      }
      this.parent();
    }
  });
});