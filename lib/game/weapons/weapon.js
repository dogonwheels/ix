ig.module(
  'game.weapons.weapon'
)
.requires(
  'impact.impact'
)
.defines(function() {
  Weapon = ig.Class.extend({
   ammo: 100,
   depleted: false,
   coolOff: new ig.Timer(),
   usesMana: true,
   fire: function() {
     if (this.ammo < 0) {
      this.depleted = true;
     }
   }
  });
});